var scene;
var clouds;
var background;
var me;
var enemy1;
var count_heart = 1;
var count_enemy = 1;
var time, timer;
var count_boom = 1;
var boom;
var gameover, gamewin;
var r;
var cornerL, cornerR, cornerT, cornerD;
var allbox;
var checkT = 1;
var dateTime = new Date().getTime();
var dropTime = -1;
//Enemy
function Enemy() {
    tEnemy = new Sprite(scene, "../../assets/game/images/enemy_level1.png", 50, 70);
    tEnemy.setPosition(631, 150);
    tEnemy.setSpeed(0);
    //enemy collideswith boom
    tEnemy.collboom = function () {
        if (this.collidesWith(boom)) {
            this.hide();
            count_enemy--;
            boom.hide();
            count_boom++;
        }
    }
    tEnemy.run = function () {
        // time = timer.getElapsedTime();
        // console.log("Time : "+time);
        console.log(dateTime)
        if (Math.floor(dateTime / 1000) % 7 == 0) {
            // if (Math.floor(time) % 2 == 0) {
            console.log("2 sce enemy walk");
            this.walk(r);
            // enemy1.checkColl();
        }
    }
    tEnemy.checkColl = function () {
        console.log("enemy Collision box");
        //predictive collision detection
        for (i = 0; i < allbox.length; i++) {
            if (this.collidesWith(allbox[i])) {
                //back up and pause
                this.x -= this.dx;
                this.y -= this.dy;
                r = Math.floor(Math.random() * 4);
                enemy1.walk(r);
            }
        }
        if (this.collidesWith(cornerL) || this.collidesWith(cornerR) || this.collidesWith(cornerT) || this.collidesWith(cornerD)) {
            this.x -= this.dx;
            this.y -= this.dy;
            r = Math.floor(Math.random() * 4);
            enemy1.walk(r);
        }
        //random enemy
        tEnemy.walk = function (r) {
            console.log("Random Enemy : " + r);
            if (r == 0) {
                //Up
                this.setSpeed(3);
                this.setMoveAngle(0);
            } else if (r == 1) {
                //Down
                this.setSpeed(3);
                this.setMoveAngle(180);
            } else if (r == 2) {
                //Left
                this.setSpeed(3);
                this.setMoveAngle(270);
            } else {
                //Right
                this.setSpeed(3);
                this.setMoveAngle(90);
            }
        }
        //end walk
    }  // end checkCollision
    return tEnemy;
}
function Gameover() {
    tGameover = new Sprite(scene, "../../assets/game/images/gameover.png", 800, 600);
    tGameover.setSpeed(0);
    tGameover.setPosition(400, 300);
    return tGameover;

}
function Gamewin() {
    tGamewin = new Sprite(scene, "../../assets/game/images/gamewin.png", 800, 600);
    tGamewin.setSpeed(0);
    tGamewin.setPosition(400, 300);
    return tGamewin;

}
function Boom() {
    tBoom = new Sprite(scene, "../../assets/game/images/boom.png", 50, 50);
    tBoom.setSpeed(0);
    tBoom.hide();


    tBoom.drop = function () {
        dropTime = dateTime;
        if (count_boom != 0) {
            this.show();
            tBoom.setBoundAction(DIE);
            this.setPosition(me.x, me.y);
            this.setAngle(me.getImgAngle());
            count_boom--;
            console.log("drop");
            console.log("count boom = " + count_boom);
        }else{
            boom.hide();
            count_boom++;
        }
    }
    return tBoom;
}
//Corner
function CornerL() {
    tcornerl = new Sprite(scene, "../../assets/game/images/left_right_corner_level_2-3.png", 20, 930);
    tcornerl.setSpeed(0);
    tcornerl.setPosition(800, 150);
    return tcornerl;
}
function CornerR() {
    tcornerl = new Sprite(scene, "../../assets/game/images/left_right_corner_level_2-3.png", 20, 930);
    tcornerl.setSpeed(0);
    tcornerl.setPosition(10, 150);
    return tcornerl;
}
function CornerT() {
    tcornerl = new Sprite(scene, "../../assets/game/images/up_down_corner_level_2-3.png", 1600, 44);
    tcornerl.setSpeed(0);
    tcornerl.setPosition(0, 0);
    return tcornerl;
}
function CornerD() {
    tcornerl = new Sprite(scene, "../../assets/game/images/up_down_corner_level_2-3.png", 1600, 40);
    tcornerl.setSpeed(0);
    tcornerl.setPosition(0, 600);
    return tcornerl;
}
//Box
function Box() {
    tBox = new Sprite(scene, "../../assets/game/images/store_level2-3.png", 50, 50);
    tBox.setSpeed(0);
    return tBox;
}
//Background
function Background() {
    tbackground = new Sprite(scene, "../../assets/game/images/bg_game1-2.png", 800, 600);
    tbackground.setSpeed(0);
    tbackground.setPosition(400, 300);
    return tbackground;
} // end ocean
//player
function ME() {
    tMe = new Sprite(scene, "../../assets/game/images/me.png", 40, 60);
    tMe.speed = 0;
    //Start
    tMe.setPosition(125, 448);
    //right
    // tMe.setPosition(667, 450);
    //UP
    // tMe.setPosition(125, 100);


    tMe.motionMove = function () {
        if (keysDown[K_LEFT]) {
            this.setSpeed(3);
            this.setMoveAngle(270);
        }
        if (keysDown[K_RIGHT]) {
            this.setSpeed(3);
            this.setMoveAngle(90);
        }
        if (keysDown[K_UP]) {
            this.setSpeed(3);
            this.setMoveAngle(0);
        }
        if (keysDown[K_DOWN]) {
            this.setSpeed(3);
            this.setMoveAngle(180);
        }
        if (keysDown[K_SPACE]) {
            console.log("boom drop");
            boom.drop();
        } // end if
        tMe.setSpeed(this.speed)
    }
    tMe.checkCollision = function () {
        //predictive collision detection
        console.log("me collisino box");
        for (i = 0; i < allbox.length; i++) {
            if (this.collidesWith(allbox[i])) {
                //back up and pause
                this.x -= this.dx;
                this.y -= this.dy;
            }
        }
        if (this.collidesWith(cornerL) || this.collidesWith(cornerR) || this.collidesWith(cornerT) || this.collidesWith(cornerD)) {
            this.x -= this.dx;
            this.y -= this.dy;
        }

    }
    // end checkCollision
    tMe.checkCollEnemy = function () {
        console.log("player collideswith enemy");
        if (this.collidesWith(enemy1)) {
            count_heart--;
        }
    }

    return tMe;
}
//checkTime
function updateTime() {
    time = timer.getElapsedTime();
    scoreTime = document.getElementById("time");
    scoreTime.innerHTML = time;
    // scoreTime.innerHTML = 0;
}
//checkEnemy
function checkEnemy() {
    scoreEnemy = document.getElementById("enemy");
    scoreEnemy.innerHTML = count_enemy;
}
function checkBoom() {
    scoreBoom = document.getElementById("boom");
    scoreBoom.innerHTML = count_boom;

}
function checkHeart() {
    scoreHeart = document.getElementById("heart");
    scoreHeart.innerHTML = count_heart;
}
//resetpage
function restart() {
    document.location.href = "";
}
function init() {
    scene = new Scene();
    timer = new Timer();
    console.log(time);
    createbox();
    scene.hideCursor();
    me = new ME();
    enemy1 = new Enemy();
    cornerL = new CornerL();
    cornerR = new CornerR();
    cornerT = new CornerT();
    cornerD = new CornerD();
    boom = new Boom();
    gameover = new Gameover();
    gameover.hide();
    gamewin = new Gamewin();
    gamewin.hide();
    background = new Background();
    scene.start();
}  // end init
function createbox() {
    console.log('Creating box')
    allbox = new Array(10)
    for (i = 0; i < allbox.length; i++) {
        allbox[i] = new Box();
        console.log("allbox = " + allbox[i]);
        // allbox[i].hide();
    }
    //1-10
    allbox[0].setPosition(263, 227);
    allbox[1].setPosition(140, 365);
    allbox[2].setPosition(200, 435);
    allbox[3].setPosition(324, 435);
    allbox[4].setPosition(324, 295);
    allbox[5].setPosition(446, 158);
    allbox[6].setPosition(570, 158);
    allbox[7].setPosition(570, 225);
    allbox[8].setPosition(570, 365);
    allbox[9].setPosition(631, 435);
}
function HideObject() {
    cornerL.hide();
    cornerR.hide();
    cornerT.hide();
    cornerD.hide();
    me.hide();
    enemy1.hide();
    boom.hide();
    for (i = 0; i < allbox.length; i++) {
        allbox[i].hide();
    }

}
function CheckStutsPlayer() {
    if (count_enemy == 0) {
        console.log("win!!!");
        gamewin.show();
        HideObject();
        // scoreState = document.getElementById("status");
        // scoreState.innerHTML = "Status : ended";
    }
    if (count_heart == 0) {
        console.log("lose???");
        gameover.show();
        HideObject();
    }
}
function update() {
    dateTime = new Date().getTime();
    r = Math.floor(Math.random() * 4);
    scene.clear();
    CheckStutsPlayer();
    background.update();
    gameover.update();
    gamewin.update();
    cornerL.update();
    cornerR.update();
    cornerT.update();
    cornerD.update();
    updateTime();
    checkHeart();
    checkBoom();
    checkEnemy();
    me.motionMove();
    me.update();
    boom.update();
    enemy1.collboom();
    enemy1.run();
    enemy1.checkColl();
    enemy1.update();
    me.checkCollision();
    me.checkCollEnemy();
    for (i = 0; i < allbox.length; i++) {
        allbox[i].update();
    }
} // end update();